const mongo = require('mongodb');
const mongoose = require('mongoose');
const categorySchema = require('../schema/category.schema');
const Category = mongoose.model('Category', categorySchema.schema);

const config = require('config');

mongoose.connect(config.get('db.host'), { useNewUrlParser: true });

// Create a new category
exports.create = (categoryData) => {
    const category = new Category(categoryData);
    return category.save();
};
