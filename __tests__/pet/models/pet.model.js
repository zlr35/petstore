const modelPet = require('../../../pet/models/pet.model');
const modelCategory = require('../../../category/models/category.model');

const testPet = {
    category : [],
    name : "Tester McTesting",
    photoUrls : [],
    tags : [],
    status: 'available'
};

describe('Create pets', () => {
    let pets = [];

    beforeAll(async () => {
        let category = await modelCategory.create({name: 'Test Category'});
        testPet.category.push(category);
    });

    afterAll(async () => {
        for (const pet of pets) {
            await modelPet.removeById(pet.id);
        };
    });

    test('Create pet', async () => {
        let pet = await modelPet.create(testPet);
        pets.push(pet);

        expect(pet.id).toMatch(/^[a-zA-z0-9]+$/);
        expect(pet.status).toMatch('available');

        expect(Array.isArray(pet.category)).toBe(true);
        expect(pet.category.length).toBeGreaterThan(0);
    });
});

describe('Get pets', () => {
    let pet;

    beforeAll(async () => {
        pet = await modelPet.create(testPet);
    });

    afterAll(async () => {
        await modelPet.removeById(pet.id);
    });

    test('Get pet by ID', async () => {
        let found = await modelPet.getById(pet.id);
        expect(found.id).toBe(pet.id);
    });

    test('Get pet by ID not found', async () => {
        await modelPet.getById('9aaaaa9999999a999999999a')
            .catch((err) => {
                expect(err.message).toBe('Pet not found');
            });
    });
});

describe('Delete pets', () => {
    let pet;

    beforeAll(async () => {
        pet = await modelPet.create(testPet);
    });

    test('Delete pet by ID', async () => {
        await expect(modelPet.removeById(pet.id)).resolves.toBeNull();
    });
});
