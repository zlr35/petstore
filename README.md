# Pet Store API

Node JS Rest API

### Run API
```
npm install
npm start
```

### Test API
```
npm test
```

### Requires
- mongodb

### Working endpoints
```
POST /pet Add a new pet to the store
GET ​/pet​/{petId} Find pet by ID
DELETE ​/pet​/{petId} Deletes a pet
```