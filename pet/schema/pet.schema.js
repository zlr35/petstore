const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const petSchema = new Schema({
    category: [{ type: Schema.Types.ObjectId, ref: 'Category' }],
    name: String,
    photoUrls: [String],
    tags: [{ type: Schema.Types.ObjectId, ref: 'Tag' }],
    status: String
});

petSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

petSchema.set('toJSON', {
    virtuals: true
});

module.exports = {
    schema: petSchema
};
