const mongo = require('mongodb');
const mongoose = require('mongoose');
const petSchema = require('../schema/pet.schema');
const Pet = mongoose.model('Pet', petSchema.schema);

const config = require('config');

mongoose.connect(config.get('db.host'), { useNewUrlParser: true });

// Add a new pet to the stsore
exports.create = (petData) => {
    const pet = new Pet(petData);
    return pet.save();
};

// Update an existing pet
exports.patch = (id, petData) => {
    return new Promise((resolve, reject) => {
        // TODO
        resolve({});
    });
};

// Find pets by status
exports.getByStatus = (status) => {
    return new Promise((resolve, reject) => {
        // TODO
        resolve({});
    });
};

// Find pet by ID
exports.getById = (id) => {
    return Pet.findById(id, {})
        .then((result) => {
            if (!result) {
                throw new Error("Pet not found");
            }

            return result.toJSON();
        });
};

// Deletes a pet
exports.removeById = (id) => {
    return new Promise((resolve, reject) => {
        Pet.deleteOne({_id: id}, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(err);
            }
        });
    });
};

// Uploads an image
exports.uploadImage = (id, petData) => {
    return new Promise((resolve, reject) => {
        // TODO
        resolve({});
    });
};

// List
exports.list = () => {
    return new Promise((resolve, reject) => {
        Pet.find({})
            .exec(function (err, pets) {
                if (err) {
                    reject(err);
                } else {
                    resolve(pets);
                }
            })
    });
};
