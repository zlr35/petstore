const PetModel = require('../models/pet.model');

// Add a new pet to the stsore
exports.insert = (req, res) => {
    PetModel.create(req.body)
        .then((result) => {
            res.status(201).send({id: result._id});
        });
};

// Update an existing pet
exports.patch = (req, res) => {
    PetModel.patch(null, req.body)
        .then((result) => {
            res.status(200).send(result);
        })
        .catch((err) => {
            res.status(502).send({message: "Error updating pet", error: err.message})
        });
};

// Find pets by status
exports.getByStatus = (req, res) => {
    PetModel.getByStatus(req.params.code)
        .then((result) => {
            res.status(200).send(result);
        })
        .catch((err) => {
            res.status(404).send({ok: false, error: err.message});
        });
};

// Find pet by ID
exports.getById = (req, res) => {
    PetModel.getById(req.params.id)
        .then((result) => {
            res.status(200).send(result);
        })
        .catch((err) => {
            res.status(404).send({ok: false, error: err.message});
        });
};

// Updates a pet in the store with form data
exports.patch = (req, res) => {
    PetModel.patch(req.params.id, req.body)
        .then((result) => {
            res.status(200).send(result);
        })
        .catch((err) => {
            res.status(502).send({message: "Error updating pet", error: err.message})
        });
};

// Deletes a pet
exports.removeById = (req, res) => {
    PetModel.removeById(req.params.id)
        .then((result) => {
            res.status(204).send({ok: true});
        })
        .catch((err) => {
            res.status(500).send({ok: false});
        });
};

// Uploads an image
exports.uploadImage = (req, res) => {
    PetModel.uploadImage(req.params.id, req.body)
        .then((result) => {
            res.status(200).send(result);
        })
        .catch((err) => {
            res.status(502).send({message: "Error uploading image", error: err.message})
        });
};

exports.list = (req, res) => {
    PetModel.list()
        .then((result) => {
            res.status(200).send(result);
        })
};
