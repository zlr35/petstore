const PetController = require('./controllers/pet.controller');

exports.routesConfig = function (app) {
    // Add a new pet to the stsore
    app.post('/pet', [
        PetController.insert
    ]);
    // Update an existing pet
    app.put('/pet', [
        PetController.patch
    ]);
    // Find pets by status
    app.get('/pet/findBystatus', [
        PetController.getByStatus
    ]);
    // Find pet by ID
    app.get('/pet/:id', [
        PetController.getById
    ]);
    // Updates a pet in the store with form data
    app.post('/pet/:id', [
        PetController.patch
    ]);
    // Deletes a pet
    app.delete('/pet/:id', [
        PetController.removeById
    ]);
    // Uploads an image
    app.post('/pet/:id/uploadImage', [
        PetController.uploadImage
    ]);
    // List
    app.get('/pets', [
        PetController.list
    ]);
};